import React, { Component } from 'react';
import { string, shape } from 'prop-types';
import queryString from 'query-string';

import ForecastDetails from './ForecastDetails';
import Loading from './Loading';
import { OPENWEATHERAPIKEY } from '../api/config';

export default class Results extends Component {
    static propTypes = {
        location: shape({
            search: string.isRequired,
        }).isRequired,
    };

    state = {
        loading: true,
        error: null,
        data: null,
    };

    componentDidMount = () => {
        this.updateData();
    };

    componentWillReceiveProps = (nextProps) => {
        const { city } = queryString.parse(nextProps.location.search);
        const { city: oldCity } = queryString.parse(this.props.location.search);
        if (city !== oldCity) {
            this.updateData({ city });
        }
    };

    updateData = ({ city } = queryString.parse(this.props.location.search)) => {
        fetch(`https://api.openweathermap.org/data/2.5/forecast/daily?q=${ formatInput(city) }&APPID=${ OPENWEATHERAPIKEY }&cnt=7&units=metric`)
        .then(res => res.json())
        .then((data) => {
            if (!data) {
                this.setState({
                    error: 'Looks like there was an error with the search',
                    loading: false,
                });
            } else {
                this.setState({
                    data,
                    loading: false,
                    error: null,
                });
            }
        });
    };

    render() {
        const { loading, error, data } = this.state;

        if (loading) {
            return <Loading />;
        }

        if (error) {
            return <h1>data not found</h1>;
        }

        return <ForecastDetails data={data} />;
    }
}

export function formatInput(input) {
    return input.split(/[,.]|\s/).filter(val => val);
}
