import React, { Component } from 'react';
import { Layout } from 'antd';
import Search from './Search';
const { Content } = Layout;

class Home extends Component {
    state = {
        input: '',
    };
    
    handleChange = (e) => {
        this.setState({ input: e.target.value });
    };
    
    render() {
        return (
            <div>
                <Layout className="layout"> 
                    <Content style={{ padding: '0 50px' }}>
                        <Search onChange={this.handleChange} input={this.state.input} />
                    </Content>
                </Layout>
            </div>
        )
    }
}

export default Home;
