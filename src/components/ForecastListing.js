import React from 'react';
import { string, number, instanceOf, object, shape } from 'prop-types';

import Moment from 'react-moment';

ForecastListing.propTypes = {
    location: shape({
        state: shape({
            day: object.isRequired,
            date: instanceOf(Date).isRequired,
            name: string.isRequired,
            maxTemp: number.isRequired,
            minTemp: number.isRequired,
            country: string.isRequired,
        }).isRequired,
    }).isRequired,
};

export default function ForecastListing(props) {
    const {
        day, date, name, maxTemp, minTemp, country,
    } = props.location.state;

    const details = (
        <div>
            <h2>{name}, {country}</h2>
            <h2>{day.weather[0].description}</h2>
            <h2>max: {maxTemp} ºC</h2>
            <h2>min: {minTemp} ºC</h2>
            <h2>humidity: {day.humidity}</h2>
        </div>
    );
    
    const dateFormated = <Moment format="dddd, MMM Do">{date}</Moment>

    return (
        <div style={{textAlign:'center'}}>
            <h1 style={{textDecoration:'underline'}}>{dateFormated}</h1>
            {details}
        </div>
    );
}
