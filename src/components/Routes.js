import React from 'react';
import { Switch, Route } from 'react-router-dom';

import NotFound from './NotFound';
import Home from './Home';
import Results from './Results';
import ForecastListing from './ForecastListing';

export default function Routes() {
    return (
        <Switch>
            <Route exact path="/" component={Home} />
            <Route path="/forecast" component={Results} />
            <Route path="/details" component={ForecastListing} />
            <Route component={NotFound} />
        </Switch>
    );
}
