import React from 'react';
import { instanceOf, object } from 'prop-types';
import Moment from 'react-moment';
import { Card, Col } from 'antd';

Forecast.propTypes = {
    date: instanceOf(Date).isRequired,
    children: object,
};

Forecast.defaultProps = {
    children: null,
};

export default function Forecast({ date, children }) {
    const dateFormated = <Moment format="dddd, MMM Do">{date}</Moment>
    return (
        <Col span={6} style={{paddingBottom:'10px'}}>
            <Card title={dateFormated} bordered={false}>
                {children}
            </Card>
        </Col>
    );
}
