import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import Forecast from '../components/Forecast';
import countryObj from '../data/city-list';
import { Row } from 'antd';

export default class ForecastDetails extends Component {
    state = {
        name: null,
        country: null,
        days: null,
    };

    componentWillMount() {
        this.updateData(this.props);
    }

    componentWillReceiveProps(nextProps) { 
        this.updateData(nextProps);
    }

    getCountry(countryCode) {
        return countryObj.find(country => country.code === countryCode).name;
    }

    updateData({ data }) {
        const { city: { name, country: countryCode }, list: days } = data;
        const country = this.getCountry(countryCode);
        this.setState({
            name,
            country,
            days,
        });
    }

    generateDate = utcSeconds => new Date(utcSeconds * 1000);

    buildForecastComponents() {
        const { name, country, days } = this.state;
        return(
            <div style={{ background: '#ECECEC', padding: '30px' }}>
                <Row gutter={16}>
                    {days.map((day) => {
                        const utcSeconds = day.dt;
                        const maxTemp = Math.round(day.temp.max);
                        const minTemp = Math.round(day.temp.min);
                        const date = this.generateDate(utcSeconds);
                        return (
                            <Link
                                key={`link-${ utcSeconds }`}
                                to={{
                                    pathname: `/details/${ name }`,
                                    state: {
                                        day,
                                        date,
                                        name,
                                        maxTemp,
                                        minTemp,
                                        country,
                                    },
                                }}
                                >
                                <Forecast key={utcSeconds} date={date}>
                                    <h1>{maxTemp} C</h1>
                                </Forecast>
                            </Link>
                        );
                    })}
                </Row>
            </div>
        );
    }

    render() {
        const {
            name, country
        } = this.state;
        
        const forecastDays = this.buildForecastComponents();
        
        return (
            <div>
                <h1 style={{textAlign:'center'}}>{name}, {country}</h1>
                {forecastDays}
            </div>
        );
    }
}
