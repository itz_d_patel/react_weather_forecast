import React from 'react';
import { string, func } from 'prop-types';
import { Link } from 'react-router-dom';
import { Input, Row, Col, Button } from 'antd';

Search.propTypes = {
    input: string,
    onChange: func,
};

Search.defaultProps = {
    input: '',
};

export default function Search(props) {
    const { input, onChange } = props;
    return (
        <div>
            <Row type="flex" justify="center" align="top">
                <Col span={10} >
                    <h3>Enter city name and get weather info</h3>
                </Col>
            </Row>
            <Row type="flex" justify="center" align="top">
                <Input
                    type="text"
                    className="form-control"
                    placeholder="Enter City Name"
                    value={input}
                    onChange={onChange}
                />
            </Row>
            <Row type="flex">
                <Button type="primary">
                    <Link
                        className={input ? '' : 'disabled'}
                        to={
                            input && {
                                pathname: '/forecast',
                                search: `?city=${ input }`,
                            }
                        }
                    >
                        Get Weather
                    </Link>
                </Button>
            </Row>
        </div>
    );
}
