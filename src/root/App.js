import React from 'react';
import Main from '../components/Main';
import { Layout } from 'antd';
const { Header } = Layout;

const App = () => (
    <div>
        <Layout className="layout">
            <Header>
                <h3 style={{ textAlign: 'center', color: '#b1b1b1', }}>Weather Demo</h3>
            </Header>
        </Layout>
        <Main />
    </div>
);

export default App;
