import React from 'react';
import { render } from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import App from './root/App';

import 'bootstrap/dist/js/bootstrap.bundle.min'
import 'bootstrap/dist/css/bootstrap.min.css'
import 'bootstrap/dist/js/bootstrap.min.js.map'
import 'antd/dist/antd.css';

render(
    <BrowserRouter>
        <App />
    </BrowserRouter>,
    document.getElementById('container'),
);
